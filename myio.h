#include <stddef.h>
#include <sys/types.h>


struct MYFILE 
{
    int file_descriptor;    // the file descriptor for the open file
    void *buf;              // the buffer for io
    off_t user_offset;      // the file offset that the user is aware of
    off_t end_of_data;      // the end of the valid data in the buffer
    off_t start_of_data;    // the beginning of the valid data in the buffer
    int open_flags;
};

struct MYFILE *myopen(char *pathname, int flags);
int myclose(struct MYFILE *file);
size_t myread(void *ptr, size_t size, struct MYFILE *file);
size_t mywrite(void *ptr, size_t size, struct MYFILE *file);
off_t myseek(struct MYFILE *file, off_t offset, int whence);
int myflush(struct MYFILE *file);
