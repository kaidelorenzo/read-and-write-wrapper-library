CFLAGS=-g -Wall -pedantic

testio: myio.o test.o
	gcc -lm -o $@ $^

%.o: %.c myio.h
	gcc $(CFLAGS) -c -o $@ $<

.PHONY: clean
clean:
	rm -f testio *.o *.test