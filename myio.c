#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdio.h>
#include <errno.h>
#include "myio.h"

#define CHUNK_SIZE 4096

size_t iohelper(void *ptr, size_t size, struct MYFILE *file, int reading);

struct MYFILE *myopen(char *pathname, int flags){
    struct MYFILE *file = malloc(sizeof(struct MYFILE));
    if (file == NULL){
        return NULL;
    }

    file->open_flags = flags;

    if((file->file_descriptor = open(pathname, flags, 0666)) == -1){
        // clean up
        free(file);
        return NULL;
    }
    file->buf = malloc(CHUNK_SIZE);
    if(file->buf == NULL){
        // clean up
        free(file);
        return NULL;
    }
    file->user_offset = 0;
    if (O_WRONLY == (file->open_flags & O_WRONLY)){
       // correct for when the file is open write only
        file->start_of_data = file->user_offset % CHUNK_SIZE;
        file->end_of_data = file->start_of_data;
        if(lseek(file->file_descriptor, file->start_of_data, SEEK_CUR) == -1){
            // clean up
            free(file->buf);
            free(file);
            return NULL;
        } 
    } else { // O_RDONLY or O_RDWR
        if((file->end_of_data = read(file->file_descriptor, file->buf, CHUNK_SIZE)) == -1){
            // clean up
            free(file->buf);
            free(file);
            return NULL;
        }
        file->start_of_data = 0;
        off_t cur_seek = lseek(file->file_descriptor, 0, SEEK_CUR);
        if(cur_seek  == -1){
            // clean up
            free(file->buf);
            free(file);
            return NULL;
        }
        if(lseek(file->file_descriptor, cur_seek - file->end_of_data, SEEK_SET) == -1){
            // clean up
            free(file->buf);
            free(file);
            return NULL;
        }
    }

    return file;
}

int myclose(struct MYFILE *file){

    myflush(file);
    if(close(file->file_descriptor) == -1){
        return EOF;
    }

    free(file->buf);
    free(file);

    return 0;
}

size_t myread(void *ptr, size_t size, struct MYFILE *file){
    return iohelper(ptr, size, file, 1);
}

size_t mywrite(void *ptr, size_t size, struct MYFILE *file){
    return iohelper(ptr, size, file, 0);
}

// helper function that merges the common actions made for read and write actions
size_t iohelper(void *ptr, size_t size, struct MYFILE *file, int reading){

    // the remaining amount of space/data in the buffer
    size_t unprocessed_bytes = CHUNK_SIZE - (file->user_offset % CHUNK_SIZE);

    // how much of the size of the requested action has been completed
    size_t amount_processed = 0;

    while(size > amount_processed){
        if(size - amount_processed > unprocessed_bytes){
            if(reading) {
                if (file->end_of_data != CHUNK_SIZE){
                    // adjust if reading past end of file
                    size = size - ((size - amount_processed) - (file->end_of_data - (file->user_offset % CHUNK_SIZE)));
                    unprocessed_bytes = file->end_of_data - (file->user_offset % CHUNK_SIZE);
                    memcpy((char*)ptr + amount_processed, (char*)file->buf + (file->user_offset % CHUNK_SIZE), size - amount_processed);
                } else {
                    memcpy((char*)ptr + amount_processed, (char*)file->buf + (file->user_offset % CHUNK_SIZE), unprocessed_bytes);
                }
            } else {
                memcpy((char*)file->buf + (file->user_offset % CHUNK_SIZE), (char*)ptr + amount_processed, unprocessed_bytes);
                if ((file->user_offset % CHUNK_SIZE) + unprocessed_bytes >= file->end_of_data){
                    file->end_of_data = (file->user_offset % CHUNK_SIZE) + unprocessed_bytes;
                }
            }

            amount_processed = amount_processed + unprocessed_bytes;
            file->user_offset = file->user_offset + unprocessed_bytes;
            myflush(file);            
            unprocessed_bytes = CHUNK_SIZE;
        }
        else{
            if(reading){
                // adjust if reading past end of file
                if (file->end_of_data - (file->user_offset % CHUNK_SIZE) < size - amount_processed){
                    size = size - ((size - amount_processed) - (file->end_of_data - (file->user_offset % CHUNK_SIZE)));
                }
                memcpy((char*)ptr + amount_processed, (char*)file->buf + (file->user_offset % CHUNK_SIZE), size - amount_processed);

            } else {
                memcpy((char*)file->buf + (file->user_offset % CHUNK_SIZE), (char*)ptr + amount_processed, size - amount_processed);
                if ((file->user_offset % CHUNK_SIZE) + (size - amount_processed) >= file->end_of_data){
                    file->end_of_data = (file->user_offset % CHUNK_SIZE) + (size - amount_processed);
                }
            }
            file->user_offset = file->user_offset + (size - amount_processed);
            unprocessed_bytes = unprocessed_bytes - (size - amount_processed);
            if(unprocessed_bytes == 0){
                myflush(file);
            }
            amount_processed = size;
        }
    }
    return amount_processed;
}

off_t myseek(struct MYFILE *file, off_t offset, int whence){
    // change offset to absolute offset if relative is given
    if (whence == SEEK_CUR){
        offset = file->user_offset + offset;
    }
    file->user_offset = offset;
    off_t cur_seek = lseek(file->file_descriptor, 0, SEEK_CUR);
    if(cur_seek  == -1){
        return -1;
    }

    // check to see if we need to flush the buffer and load new buffer
    if (!(cur_seek + file->start_of_data <= offset && offset < cur_seek + file->end_of_data)){
        myflush(file);
        if (lseek(file->file_descriptor, offset - (offset % CHUNK_SIZE), SEEK_SET) == -1){
            return -1;
        }
        
        if (O_WRONLY == (file->open_flags & O_WRONLY)){
            // correct for when the file is open write only
            file->start_of_data = file->user_offset % CHUNK_SIZE;
            file->end_of_data = file->start_of_data;
            if (lseek(file->file_descriptor, file->start_of_data, SEEK_CUR) == -1){
                return -1;
            }
        } else { // O_RDONLY or O_RDWR
            if((file->end_of_data = read(file->file_descriptor, file->buf, CHUNK_SIZE)) == -1){
                return -1;
            }
            file->start_of_data = 0;
            off_t cur_seek = lseek(file->file_descriptor, 0, SEEK_CUR);
            if(cur_seek  == -1){
                return -1;
            }
            if (lseek(file->file_descriptor, cur_seek - file->end_of_data, SEEK_SET) == -1){
                return -1;
            }
        }
    }

    return offset;
}
int myflush(struct MYFILE *file){
    if (O_WRONLY == (file->open_flags & O_WRONLY) || O_RDWR == (file->open_flags & O_RDWR)){
        if (write(file->file_descriptor, (char *)file->buf + file->start_of_data, file->end_of_data - file->start_of_data) == -1){
            return EOF;
        }
    } else { // O_RDONLY
        // correct for when the file is open read only
        if (lseek(file->file_descriptor, file->user_offset, SEEK_SET) == -1){
            return EOF;
        }
    }

    if (lseek(file->file_descriptor, file->user_offset - (file->user_offset % CHUNK_SIZE), SEEK_SET) == -1){
        return EOF;
    }
    if (O_WRONLY == (file->open_flags & O_WRONLY)){
        // correct for when the file is open write only
        file->start_of_data = file->user_offset % CHUNK_SIZE;
        file->end_of_data = file->start_of_data;
        if (lseek(file->file_descriptor, file->start_of_data, SEEK_CUR) == -1){
            return -1;
        }
    } else { // O_RDONLY or O_RDWR
        if((file->end_of_data = read(file->file_descriptor, file->buf, CHUNK_SIZE)) == -1){
            return -1;
        }
        file->start_of_data = 0;
        off_t cur_seek = lseek(file->file_descriptor, 0, SEEK_CUR);
        if(cur_seek  == -1){
            return -1;
        }
        if (lseek(file->file_descriptor, cur_seek - file->end_of_data, SEEK_SET) == -1){
            return -1;
        }
    }

    return 0;
}